LibAO Plug-in
=============

Overview
--------

This plug-in adds support for [JACK Audio Connection Kit](http://jackaudio.org/) in [libao](https://www.xiph.org/ao/).

There is a [request to include the patch](https://trac.xiph.org/ticket/2083) in the library. But it seems to be unmaintained.

Compilation
-----------

To compile the plugin, fetch the libao version 1.2.0.

    $ cd /path/to/libao
	$ cp -a /path/to/libao-jack-plugin/src/plugins/jack src/plugins/
	$ patch -p1 < /path/to/libao-jack-plugin/patch/0001-jack-plugin.patch
	$ patch -p1 < /path/to/libao-jack-plugin/patch/0002-rename-libjack.patch
    $ aclocal
    $ automake --add-missing
    $ autoconf
    $ ./configure --prefix=/usr --disable-alsa --disable-oss --disable-pulse \
	$     --disable-arts --disable-esd --disable-roar
    $ make DESTDIR=/path/to/build

Then copy _/path/to/build/usr/lib/ao/plugins-4/libjackdriver.so_ to _/usr/lib/ao/plugins-4/libjack.so_.

Configuration
-------------

Example of configuration file:

    default_driver=jack
    ports=system:playback_1,system:playback_2
    quality=5
